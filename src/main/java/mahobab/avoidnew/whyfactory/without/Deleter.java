package mahobab.avoidnew.whyfactory.without;

import java.io.File;

public class Deleter {

    public void delete(String path) {
        File file = new File(path);
        file.delete();
    }
}
