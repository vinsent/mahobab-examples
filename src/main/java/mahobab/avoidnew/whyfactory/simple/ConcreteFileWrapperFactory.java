package mahobab.avoidnew.whyfactory.simple;

import java.io.File;

public class ConcreteFileWrapperFactory implements FileWrapperFactory {
    public FileWrapper create(String path) {
        return new ConcreteFileWrapper(new File(path));
    }
}
