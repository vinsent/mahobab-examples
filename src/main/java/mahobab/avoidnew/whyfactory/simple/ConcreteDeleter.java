package mahobab.avoidnew.whyfactory.simple;

import com.google.inject.Singleton;

import javax.inject.Inject;

@Singleton
public class ConcreteDeleter implements Deleter {
    private final FileWrapperFactory fileWrapperFactory;

    @Inject
    public ConcreteDeleter(FileWrapperFactory fileWrapperFactory) {
        this.fileWrapperFactory = fileWrapperFactory;
    }

    public void delete(String path) {
        FileWrapper file = this.fileWrapperFactory.create(path);
        if(!file.delete()) throw new CouldNotDeleteFileException();
    }
}
