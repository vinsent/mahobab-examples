package mahobab.avoidnew.whyfactory.simple;

/**
 * Created by alunlun on 7/8/2015.
 */
public interface FileWrapper {
    boolean delete();
}
