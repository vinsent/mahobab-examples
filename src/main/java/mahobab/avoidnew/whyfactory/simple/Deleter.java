package mahobab.avoidnew.whyfactory.simple;

public interface Deleter {
    void delete(String path);
}
