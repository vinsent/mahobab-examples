package mahobab.avoidnew.whyfactory.simple;

import java.io.File;

/**
 * Created by alunlun on 7/8/2015.
 */
public class ConcreteFileWrapper implements FileWrapper {
    private final File file;

    public ConcreteFileWrapper(File file) {
        this.file = file;
    }

    public boolean delete() {
        return file.delete();

    }
}
