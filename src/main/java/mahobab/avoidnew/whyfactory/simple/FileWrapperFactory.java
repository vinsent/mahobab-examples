package mahobab.avoidnew.whyfactory.simple;

public interface FileWrapperFactory {
    FileWrapper create(String path);
}
