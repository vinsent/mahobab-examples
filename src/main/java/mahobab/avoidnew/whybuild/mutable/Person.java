package mahobab.avoidnew.whybuild.mutable;

import java.util.Set;

public class Person {
    private String name;
    private int idNumber;
    private Person bestFriend;
    private Set<Person> family;


    public Set<Person> getFamily() {
        return family;
    }

    public void setFamily(Set<Person> family) {
        this.family = family;
    }

    public Person getBestFriend() {
        return bestFriend;
    }

    public void setBestFriend(Person bestFriend) {
        this.bestFriend = bestFriend;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

}
