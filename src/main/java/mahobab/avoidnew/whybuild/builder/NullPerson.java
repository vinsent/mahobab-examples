package mahobab.avoidnew.whybuild.builder;

import java.util.Collections;
import java.util.Set;

public class NullPerson implements Person {
    private final boolean isNullPerson;

    public NullPerson() {
        isNullPerson = true;
    }

    public Set<Person> getFamily() {
        return Collections.emptySet();
    }

    public Person getBestFriend() {
        return new NullPerson();
    }

    public String getName() {
        return "";
    }

    public int getIdNumber() {
        return 0;
    }

    public boolean isNullPerson() {
        return this.isNullPerson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NullPerson that = (NullPerson) o;

        return isNullPerson == that.isNullPerson;

    }

    @Override
    public int hashCode() {
        return (isNullPerson ? 1 : 0);
    }
}
