package mahobab.avoidnew.whybuild.builder;

import java.util.Set;

public interface Person {
    Set<Person> getFamily();

    Person getBestFriend();

    String getName();

    int getIdNumber();

    boolean isNullPerson();
}
