package mahobab.avoidnew.whybuild.builder;

import java.util.Collections;
import java.util.Set;

public class RealPerson implements Person {


    private final String name;
    private final int id;
    private final Person friend;
    private final Set<Person> family;

    private RealPerson(String name, int id, Person friend, Set<Person> family) {
        this.name = name;
        this.id = id;
        this.family = family;
        this.friend = friend;
    }

    public Set<Person> getFamily() {
        return family;
    }

    public Person getBestFriend() {
        return friend;
    }

    public String getName() {
        return name;
    }

    public int getIdNumber() {
        return id;
    }

    public boolean isNullPerson() {
        return false;
    }

    public static class Builder {

        private final String name;
        private final int id;
        private Person friend;
        private Set<Person> family;

        public Builder(String name, int id) {
            this.id = id;
            this.name = name;
            this.family = Collections.emptySet();
            this.friend = new NullPerson();
        }

        public Person build() {
            return new RealPerson(name, id, friend, family);
        }

        public Builder bestFriend(Person friend) {
            this.friend = friend;
            return this;
        }

        public Builder family(Set<Person> family) {
            this.family = family;
            return this;
        }
    }
}
