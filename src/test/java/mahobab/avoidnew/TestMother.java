package mahobab.avoidnew;

/**
 * Created by alunlun on 7/8/2015.
 */
public interface TestMother {
    String getAcceptedPath();

    String getBadPath();
}
