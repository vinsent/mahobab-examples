package mahobab.avoidnew;

/**
 * Just exists for testing.
 */
public class Toggle {
    private boolean b;

    public Toggle(boolean b) {
        this.b = b;
    }

    public void toggle(boolean b) {
        this.b = b;
    }

    public boolean booleanValue() {
        return b;
    }
}
