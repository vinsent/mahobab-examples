package mahobab.avoidnew;

/**
 * Created by alunlun on 7/8/2015.
 */
public class ConcreteTestMother implements TestMother{
    public String getAcceptedPath() {
        return "C:\\hoj.txt";
    }

    public String getBadPath() {
        return "jaseliasje";
    }
}
