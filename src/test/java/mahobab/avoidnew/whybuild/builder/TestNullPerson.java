package mahobab.avoidnew.whybuild.builder;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by alunlun on 7/8/2015.
 */
public class TestNullPerson {
    private Person person;

    @Before
    public void setUp(){
        this.person=new NullPerson();
    }

    @Test
    public void test(){
        boolean hasNoFamily = person.getFamily().isEmpty();
        boolean hasNoName = person.getName().isEmpty();
        boolean hasNoId = person.getIdNumber() == 0;
        boolean hasNoBestFriend = person.getBestFriend().equals(new NullPerson());
        assertTrue(hasNoBestFriend && hasNoName && hasNoFamily && hasNoId);
    }
}
