package mahobab.avoidnew.whybuild.builder;


import mahobab.avoidnew.SetAdder;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by alunlun on 7/7/2015.
 */
public class TestBuilder {

    private Person bill;
    private Person bull;

    @Before
    public void setUp() {
        SetAdder<Person> adder = new SetAdder<Person>();

        this.bull = new RealPerson.Builder("Bull", 951951).build();
        Person billsFather = new RealPerson.Builder("Grand bill", 3445).build();
        Person billsSister = new RealPerson.Builder("Billa", 15566767).build();
        Set<Person> family = adder.toSet(billsFather, billsSister);
        this.bill = new RealPerson.Builder("Bill", 5454523).bestFriend(bull).family(family).build();


    }

    @Test
    public void testMutable() {
        boolean billHasTwoFamilyMembers = bill.getFamily().size() == 2;
        boolean billsBestFriendIsBull = bill.getBestFriend().getName() == "Bull";
        boolean billsNameIsBill = bill.getName() == "Bill";
        boolean billHasCorrectId = bill.getIdNumber() == 5454523;

        boolean bullHasNoBestFriend = bill.getBestFriend().getBestFriend().isNullPerson(); //Most simple way of implementing solutions, still very much better than what is currently available.

        boolean bullHasNoFamily = bill.getBestFriend().getFamily().isEmpty();
        assertTrue(billHasTwoFamilyMembers && billsBestFriendIsBull && billsNameIsBill && billHasCorrectId && bullHasNoBestFriend && bullHasNoFamily);

        boolean billIsNoNullPerson =  !bill.isNullPerson();
        boolean bullsFamilyIsHisBestFriend = bull.getBestFriend() == bull.getFamily(); //will be true, that does not make sense
        System.out.println(bullsFamilyIsHisBestFriend);

    }
}
