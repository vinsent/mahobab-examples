package mahobab.avoidnew.whybuild.mutable;


import mahobab.avoidnew.SetAdder;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static junit.framework.TestCase.assertTrue;


public class TestMutable {

    private Person bill;
    private Person bull;

    @Before
    public void setUp() {
        SetAdder<Person> adder = new SetAdder<Person>();

        this.bull = new Person();
        bull.setName("Bull");
        bull.setIdNumber(31241);
        this.bill = new Person();
        bill.setName("Bill");
        bill.setIdNumber(5454523);
        bill.setBestFriend(bull);
        Person billsSister = new Person();
        billsSister.setName("billa");
        billsSister.setIdNumber(123213);
        Person billsFather = new Person();
        billsFather.setName("grand bill");
        billsFather.setIdNumber(123213333);
        Set<Person> family = adder.toSet(billsFather, billsSister);
        bill.setFamily(family);
    }


    @Test
    public void testMutable() {
        boolean billHasTwoFamilyMembers = bill.getFamily().size() == 2;
        boolean billsBestFriendIsBull = bill.getBestFriend().getName() == "Bull";
        boolean billsNameIsBill = bill.getName() == "Bill";
        boolean billHasCorrectId = bill.getIdNumber() == 5454523;
        boolean bullHasNoBestFriend = bull.getBestFriend() == null; //this is hoooooorrrible, implicit null logic => stone age thinking.
        boolean bullHasNoFamily = bull.getFamily() == null; // also veryyyyy horrible
        assertTrue(billHasTwoFamilyMembers && billsBestFriendIsBull && billsNameIsBill && billHasCorrectId && bullHasNoBestFriend && bullHasNoFamily);

        //Just a minor detail that sucks about null

        boolean bullsFamilyIsHisBestFriend = bull.getBestFriend() == bull.getFamily();//will be true, that does not make sense.

    }

}
