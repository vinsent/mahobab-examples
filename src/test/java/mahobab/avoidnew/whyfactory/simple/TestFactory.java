package mahobab.avoidnew.whyfactory.simple;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import mahobab.avoidnew.ConcreteTestMother;
import mahobab.avoidnew.TestMother;
import mahobab.avoidnew.Toggle;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.inject.Singleton;

import static junit.framework.TestCase.assertTrue;

public class TestFactory {

    private Deleter deleter;
    private MockFileFactory factory;
    private String badPath;
    private String acceptedPath
            ;

    @Before
    public void setUp() {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(FileWrapperFactory.class).to(MockFileFactory.class);
                bind(Deleter.class).to(ConcreteDeleter.class);
            bind(TestMother.class).to(ConcreteTestMother.class);
            }
        });
        this.deleter = injector.getInstance(Deleter.class);
        this.factory = (MockFileFactory) injector.getInstance(FileWrapperFactory.class);
        TestMother testMother = injector.getInstance(TestMother.class);
        this.acceptedPath=testMother.getAcceptedPath();
        this.badPath=testMother.getBadPath();

    }


    @Test
    public void testSimpleFactory() {
        this.deleter.delete(acceptedPath);
        assertTrue(factory.fileIsDeleted());
    }

    @Test(expected = CouldNotDeleteFileException.class)
    public void testFail() {
        this.deleter.delete(badPath);
    }

    @Singleton
    private static class MockFileFactory implements FileWrapperFactory {
        private final Toggle hasDeleted;

        private final String acceptedPath;

        @Inject
        public MockFileFactory(TestMother testMother) {
            hasDeleted = new Toggle(false);
    this.acceptedPath=testMother.getAcceptedPath();
        }

        public FileWrapper create(String path) {
            if(!path.equals(acceptedPath)){
                return new FileWrapper() {
                    public boolean delete() {
                        return false;
                    }
                };
            }
            return new FileWrapper() {
                public boolean delete() {
                    hasDeleted.toggle(true);
                    return true;
                }
            };
        }

        public boolean fileIsDeleted() {
            return hasDeleted.booleanValue();
        }
    }

}
