package mahobab.avoidnew;

import java.util.HashSet;
import java.util.Set;

/**
 * Silly tool that is used to create sets.
 *
 * @param <T>
 */
public class SetAdder<T> {

    public Set<T> toSet(T... ts) {
        Set<T> set = new HashSet<T>();
        for (T t : ts) {
            set.add(t);
        }
        return set;
    }

}
